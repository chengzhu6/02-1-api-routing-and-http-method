package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
public class BooksController {

    @GetMapping("/users/userId/books")
    public String getBookTest() {
        return String.format("The book for user %s","is true");
    }


    @GetMapping("/users/{userId}/books")
    public String getBook(@PathVariable int userId) {
        return String.format("The book for user %s", userId);
    }

    @GetMapping("/users/{userid}/book")
    public String getBookWithUserId(@PathVariable int userId) {
        return String.format("The book for user %s", userId);
    }


    @GetMapping("/segments/good")
    public String getSegments() {
        return "It's the first.";
    }

    @GetMapping("/segments/{segmentName}")
    public String getSegments(@PathVariable String segmentName) {
        return segmentName;
    }

    @GetMapping("/segments/get/?ad")
    public String getBadSegment() {
        return "successful";
    }

    @GetMapping("/wildcards/*")
    public String getWildcards() {
        return "successful";
    }

    @GetMapping("/*/wildcards")
    public String getAnyWildcards() {
        return "successful";
    }

    @GetMapping("/wildcard/before/*/after")
    public String getBeforeAfterWildcards() {
        return "successful";
    }

    @GetMapping("/wildcard/*o*/after")
    public String getAnyAfterWildcards() {
        return "mathch *o*";
    }

    @GetMapping("/wildcard/**/after/double")
    public String getDoubleWildcards() {
        return "mathch **";
    }

    @GetMapping("/regx/d$")
    public String getRegxUse() {
        return "match regx";
    }


    @GetMapping("/get/user?username=")
    public String getUser() {
        return "match query String";
    }

    @GetMapping("/get/userinfo")
    public String getUser(@RequestParam("username")  String userName) {
        return "match query String";
    }

    @GetMapping(value = "/set/password" ,params = "!newpassword")
    public String resetPassword() {
        return "match query String";
    }






}