package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
class BooksControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_retrun_200_with_book_for_request_user_idTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/userId/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 15"));

    }

    @Test
    void should_retrun_200_with_book_for_request_user_id() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/15/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 15"));

    }


    @Test
    void should_return_error_with_different_path_variable() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/15/book"))
                .andExpect(MockMvcResultMatchers.status().is(500));
    }


    @Test
    void should_return_200_when_hava_two_same_path() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("It's the first."));
    }

    @Test
    void should_return_200_when_hava_two_char_macth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/get/bbad"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_404_when_hava_two_no_macth() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/get/ad"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_200_when_hava_one_segment() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }


    @Test
    void should_return_200_when_hava_one_segment_in_intermediate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/anything/wildcards"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_404_when_no_segment_in_intermediate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/before/after"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_200_when_match_segment_in_intermediate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/good/after"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("mathch *o*"));
    }

    @Test
    void should_return_200_when_match_double_segment_in_intermediate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/good/before/after/double"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("mathch **"));
    }

    @Test
    void should_return_404_when_use_regx_match_path() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/regx/good"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_404_when_use_query_string_path() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/get/user?username=zhangsan"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_400_when_without_para() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/get/userinfo"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_with_para() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/set/password?newpassword=123"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}