package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BooksControllerUnitTest {


    private BooksController booksController = new BooksController();


    @Test
    void should_return_user_book_by_user_id() {
        String book = booksController.getBook(15);
        assertEquals("The book for user 15", book);
    }
}